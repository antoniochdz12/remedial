from django.urls import path
from . import views

app_name = 'nfl' 

urlpatterns = [
    path('duenio/create/', views.CreateDuenio.as_view(), name='create_duenio'),
    path('duenio/list/', views.ListDuenios.as_view(), name='list_duenio'),
    path('duenio/<int:pk>/', views.DetailDuenio.as_view(), name='detail_duenio'),
    path('duenio/<int:pk>/update/', views.UpdateDuenio.as_view(), name='update_duenio'),
    path('duenio/<int:pk>/delete/', views.DeleteDuenio.as_view(), name='delete_duenio'),

    # URLs para Ciudad
    path('ciudad/create/', views.CreateCiudad.as_view(), name='create_ciudad'),
    path('ciudad/list/', views.ListCiudades.as_view(), name='list_ciudades'),
    path('ciudad/<int:pk>/', views.DetailCiudad.as_view(), name='detail_ciudad'),
    path('ciudad/update/<int:pk>/', views.UpdateCiudad.as_view(), name='update_ciudad'),
    path('ciudad/<int:pk>/delete/', views.DeleteCiudad.as_view(), name='delete_ciudad'),

    # URLs para Equipo
    path('equipo/create/', views.CreateEquipo.as_view(), name='create_equipo'),
    path('equipo/list/', views.ListEquipos.as_view(), name='list_equipo'),
    path('equipo/<int:pk>/', views.DetailEquipo.as_view(), name='detail_equipo'),
    path('equipo/<int:pk>/update/', views.UpdateEquipo.as_view(), name='update_equipo'),
    path('equipo/<int:pk>/delete/', views.DeleteEquipo.as_view(), name='delete_equipo'),

    # URLs para Estadio
    path('estadio/create/', views.CreateEstadio.as_view(), name='create_estadio'),
    path('estadio/list/', views.ListEstadios.as_view(), name='list_estadio'),
    path('estadio/<int:pk>/', views.DetailEstadio.as_view(), name='detail_estadio'),
    path('estadio/<int:pk>/update/', views.UpdateEstadio.as_view(), name='update_estadio'),
    path('estadio/<int:pk>/delete/', views.DeleteEstadio.as_view(), name='delete_estadio'),

    # URLs para Jugador
    path('jugador/create/', views.CreateJugador.as_view(), name='create_jugador'),
    path('jugador/list/', views.ListJugadores.as_view(), name='list_jugador'),
    path('jugador/<int:pk>/', views.DetailJugador.as_view(), name='detail_jugador'),
    path('jugador/<int:pk>/update/', views.UpdateJugador.as_view(), name='update_jugador'),
    path('jugador/<int:pk>/delete/', views.DeleteJugador.as_view(), name='delete_jugador'),
]